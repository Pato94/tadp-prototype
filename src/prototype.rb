class PrototypeObject
  attr_accessor :singleton_module

  def initialize
    self.singleton_module = Module.new
    self.singleton_class.send :include, self.singleton_module
  end

  def set_prototype(prototype)
    self.singleton_module.send :include, prototype.singleton_module
    self.singleton_class.send :include, self.singleton_module
  end

  def set_property(sym, val)
    self.singleton_module.send :attr_accessor, sym
    self.send "#{sym}=".to_sym, val
  end

  def set_method(sym, proc)
    self.singleton_module.send(:define_method, sym, proc)
  end
end